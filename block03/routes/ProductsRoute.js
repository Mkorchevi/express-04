const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/ProductsController');

//  == This route will give us back all productsdb: ==  //

router.get('/', controller.findAll);

//  == This route will give us back one productsdb, it will be that with the id we are providing: ==  //

router.get('/:product_id', controller.findOne);

//  == This route allow us to add an extr productsdb: ==  //

router.post('/new', controller.insert);

//  == This route allow us to delete one productsdb t will be that with the id we are providing: ==  //

router.post('/delete', controller.delete);

//  == This route allow us to update one productsdb t will be that with the id we are providing ==  //

router.post('/update', controller.update);

module.exports = router;