const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const productsSchema = new Schema({
    product:String,
    price: Number,
    category_Id: String,
})
module.exports =  mongoose.model('products', productsSchema);