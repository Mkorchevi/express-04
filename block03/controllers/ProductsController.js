const Products = require('../models/ProductsModel');

class ProductsController {
    // GET FIND ALL
    async findAll(req, res){
        try{
            const products = await Products.find({});
            res.send(products);
        }
        catch(e){
            res.send({e})
        }
    }
    // FIND ONE productsdb BY _ID
    async findOne(req ,res){
        let { product_id} = req.params;
        try{
            const product = await Products.findOne({_id:product_id});
            res.send(product);
        }
        catch(e){
            res.send({e})
        }

    }
    // POST ADD ONE
    async insert (req, res) {
        let { product, price } = req.body;
        try{
            const done = await Products.create({product, price});
            res.send(done)
        }
        catch(e){
            res.send({e})
        }
    }
    // DELETE productsdb
    async delete (req, res){
        console.log('delete!!!')
        let { product } = req.body;
        try{
            const removed = await Products.deleteOne({ product });
            res.send({removed});
        }
        catch(error){
            res.send({error});
        };
    }
    // UPDATE productsdb

    async update (req, res){
        let { product, newProduct } = req.body;
        try{
            const updated = await Categories.updateOne(
                { product },{ product:newProduct }
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }


};
module.exports = new ProductsController();