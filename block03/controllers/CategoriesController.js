const Categories = require('../models/CategoriesModel');

class CategoriesController {
    // GET FIND ALL
    async findAll(req, res){
        try{
            const categories = await Categories.find({});
            res.send(categories);
        }
        catch(e){
            res.send({e})
        }
    }
    // FIND ONE productsdb BY _ID
    async findOne(req ,res){
        let { category_id} = req.params;
        try{
            const category = await Categories.findOne({_id:category_id});
            res.send(category);
        }
        catch(e){
            res.send({e})
        }

    }
    // POST ADD ONE
    async insert (req, res) {
        let { category } = req.body;
        try{
            const done = await categories.create({category});
            res.send(done)
        }
        catch(e){
            res.send({e})
        }
    }
    // DELETE productsdb
    async delete (req, res){
        console.log('delete!!!')
        let { category } = req.body;
        try{
            const removed = await Categories.deleteOne({ category });
            res.send({removed});
        }
        catch(error){
            res.send({error});
        };
    }
    // UPDATE productsdb

    async update (req, res){
        let { category, newCategory } = req.body;
        try{
            const updated = await Categories.updateOne(
                { category },{ category:newCategory }
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }


};
module.exports = new CategoriesController();