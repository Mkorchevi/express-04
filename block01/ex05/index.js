const express = require('express')
const app = express()

const port = process.env.PORT || 3001

var bankAccount = {
    '1': 1000 
}

app.get('/account/new/:accountID/:amount',(req, res)=>{
    console.log('new accountID anount')
    let id = req.params.accountID
    let amoun = req.params.amount
    if (id in bankAccount){
        res.send('AccountID already exists')
    }else{
        bankAccount[id] = amoun
        res.set('New AccountID added')
    }
})
app.get('/:accountID/withdraw/:amount',(req, res)=>{
    console.log('Withdraw amount')
    let id = req.params.accountID
    let amoun = req.params.amount
    if (bankAccount[id] === undefined){
        res.send('Account not found')
    }else{
        const deposit = bankAccount[id]
        bankAccount[id] = deposit - Number(amoun)
        res.send(bankAccount)
    }
})
app.get('/:accountID/deposit/:amount',(req, res)=>{
    console.log('Deposit amount')
    let id = req.params.accountID
    let amoun = req.params.amount
    if (bankAccount[id] === undefined){
        res.send('Account not found')
    }else{
        const balance = bankAccount[id]
        bankAccount[id] = balance + Number(amoun)
        res.send(bankAccount)
    }
})
app.get('/:accountID/balance',(req, res)=>{
    console.log('Balance')
    let id = req.params.accountID
    const balance = bankAccount[id]
    if (bankAccount[id] === undefined){
        res.send('Account not found')
    }else{
        res.send({balance:balance})
    }
})
app.get('/:accountID/delete',(req, res)=>{
    console.log('Delete')
    let id = req.params.accountID
    if (bankAccount[id] === undefined){
        res.send('Account not found')
    }else{
        delete bankAccount[id]
        res.send({message:'Bank account deleted',bankAccount})
    }   
})
app.listen(port, ()=>{
    console.log('serving my master on port', port)
})  