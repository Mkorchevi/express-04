const express = require('express')
const app = express()

const port = process.env.PORT || 3001

var multilanguages = {
    NL: 'Hallo Wereld',
    HI: 'नमस्ते दुनिया',
    FR: 'Bonjour le monde',
    ES: 'Hola Mundo',
    IT: 'Ciao Mondo',
    CH: '你好，世界',
    JP: 'こんにちは世界',
    AR: 'مرحبا بالعالم',    
    EN: 'Hello world'
}

app.get('/showmultilanguage', (req, res)=>{
    res.send(multilanguages)
})

app.get('/showlanguage/:language', (req,res)=>{
   console.log('language in obj')
   var key = req.params.language
   if (multilanguages[key] === undefined){
     res.send(multilanguages.EN)
   }else{
     res.send(multilanguages[key])
   }
 })

 app.get('/addlanguage/:newlanguage/:newmessage', (req, res)=>{
    console.log('adding language')
    let key = req.params.newlanguage
    let value = req.params.newmessage
    if( key in multilanguages){
        res.send('language already exists')
    }else{
        multilanguages[key] = value
        res.send('language added')
    }
}) 

 app.get('/removelanguage/:deletelanguage', (req, res)=> {
     console.log('remove a language')
     let key = req.params.deletelanguage
     if(multilanguages[key] === undefined){
        res.send(`Language not found`)
     }else{
     delete multilanguages[key]
     res.send(multilanguages)
    }
 })


app.listen(port, ()=>{
    console.log('serving my master on port ', port)
})