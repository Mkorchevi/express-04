const express = require('express')
const app = express();

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

let port = process.env.PORT || 3001

const DB = [
    {
        category: 't-shirts',
        products: [
            {
                name: 'blue t-shirt',
                price: 20
            },
            {
                name: 'red t-shirt',
                price: 25
            },
            {
                name: 'yellow t-shirt',
                price: 22
            },
        ]
    },
    {
        category: 'shoes',
        products: [
            {
                name: 'black shoes',
                price: 120
            },
            {
                name: 'brows shoes',
                price: 95
            },
        ]
    },
]

app.post('/category/add', (req, res)=>{ 
    console.log('Add a  new category ==>', req.body)
    var newcategory = req.body.category
    var idx = DB.findIndex(category => category.category === newcategory)
    if(idx !== -1){
        return res.send('Sorry, category already exists')
    }
    DB.push({category: newcategory,
        products: []})
    res.send(DB)
})

app.delete('/category/delete', (req, res)=>{ 
    var { category: toRemove } = req.body
    console.log('Remove category ==>', toRemove)
    var idx = DB.findIndex(category => category.category === toRemove)
    if(idx != -1){
        DB.splice(idx,1)
        res.send(DB)
    }else{
        res.send(`${toRemove} is not in the list`)
    }
})

app.post('/category/update', (req, res)=>{ 
    console.log('Update category ==>', req.body)
    var oldcategory = req.body.oldcategory
    var newcategory = req.body.newcategory
    var idx = DB.findIndex(category => category.category === oldcategory)
    if(idx !== -1){
        console.log('==========>',DB[idx])
        DB[idx].category = newcategory
        res.send(DB)
    }else{
        res.send(`${oldcategory} is not in the list`)
    }
})

app.get('/category/categories', (req, res)=>{ 
    console.log('Display all categories')
    const categories = []
    DB.forEach( ele => {
       categories.push(ele.category)
    })
    res.send(categories)
})

app.get('/category/products', (req, res)=>{
    console.log('Display all categories with all products')
    res.send(DB)
})

app.get('/category/:category', (req, res)=>{ 
    console.log('Display one category with all its products')
    var {category: findCategory} = req.params
    var idx = DB.findIndex(category => category.category === findCategory)
    if (idx === -1){
        res.send('Cannot found Category')
      }else{
        res.send(DB[idx])
      }
})

app.post('/product/add', (req, res)=>{
    console.log('Add product ==>', req.body)
    var product = req.body.product
    var price = req.body.price
    var category = req.body.category
    var idx = DB.findIndex(cat => cat.category === category)
    if (idx === -1){
        res.send('Cannot found Category')
    }else{
        var prodIdx = DB[idx].products.findIndex(pro => pro.name === product)
        if(prodIdx === -1){
            DB[idx].products.push({
                name: product,
                price: Number(price)
            })
            res.send(DB)
        }else{
            res.send('Product already exists')
        }
    }
})

app.delete('/product/delete', (req, res)=>{
    console.log('Delete product ==>', req.body)
    var product = req.body.product
    var category = req.body.category
    var idx = DB.findIndex(cat => cat.category === category)
    if(idx != -1){
        var prodIdx = DB[idx].products.findIndex(pro => pro.name === product)
        if(prodIdx !== -1){
            DB[idx].products.splice(prodIdx,1)
            res.send(DB)
        }else{
            res.send('Product doesn t exists')
        }
    }else{
        res.send(`${category} is not in the list`)
    }
})

app.post('/product/update', (req, res)=>{
    console.log('Update name, price, description ==>', req.body)
    var category = req.body.category
    var product = req.body.product
    var key = req.body.key
    var value = req.body.value
    var idx = DB.findIndex(cat => cat.category === category)
    if(idx != -1){
        var prodIdx = DB[idx].products.findIndex(pro => pro.name === product)
        if(prodIdx !== -1){
            DB[idx].products[prodIdx][key] = key === 'price' ? Number(value) : value
            res.send(DB)
        }else{
            res.send('Product doesn t exists')
        }
    }else{
        res.send(`${category} is not in the list`)
    }
})

app.listen(port,()=>{console.log('Serving on port', port)})